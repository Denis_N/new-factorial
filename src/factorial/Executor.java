package factorial;

import java.io.IOException;

/**
 * starts the program
 */
public class Executor {
   private SumOfTheFactorial sumOfTheFactorial;

   public Executor(SumOfTheFactorial sumOfTheFactorial) {
      this.sumOfTheFactorial = sumOfTheFactorial;
   }

   public static void main(String[] args) throws IOException {
      NumbersInput numbersInput = new NumbersInput(new Validator());
      Executor executor = new Executor(new SumOfTheFactorial(numbersInput));

      numbersInput.enteringStringNumber();
      executor.sumOfTheFactorial.findingTheSumOfTheFactorial();
   }
}

