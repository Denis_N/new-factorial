package factorial;

import java.math.BigInteger;
import static java.math.BigInteger.ONE;
import static java.math.BigInteger.ZERO;
import static java.math.BigInteger.valueOf;

/**
 * Calculates the factorial of a number
 */

class Factorial {
    static BigInteger factorialCalculation(int numberOfTypeInt) {
      final int numberOfTypeInt_ZERO = 0;
      int INDEX = 1;
      BigInteger numberOfTypeBigInteger = ONE;
      if (numberOfTypeInt < numberOfTypeInt_ZERO) {
          return ZERO;
      } else {
         for (;INDEX <= numberOfTypeInt; INDEX++) {
           numberOfTypeBigInteger = numberOfTypeBigInteger.multiply(valueOf(INDEX));
         }
      }
      System.out.println("Is the factorial ->" + numberOfTypeBigInteger);
    return numberOfTypeBigInteger;
   }
}
