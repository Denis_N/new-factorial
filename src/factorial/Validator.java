package factorial;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * It is used to check the input character
 */

class Validator {
     boolean checkingInputCharacters(String numberInputString) {
        Pattern pattern = Pattern.compile("(-|\\+)?\\d+");
        Matcher matcher = pattern.matcher(numberInputString);
        if (!matcher.matches()) {
            System.out.println("Input error");
        }
     return matcher.matches();
     }
}
