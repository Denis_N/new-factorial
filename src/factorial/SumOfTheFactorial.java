package factorial;

import java.math.BigInteger;
import static factorial.Factorial.factorialCalculation;
import static java.lang.Integer.parseInt;

/**
 * It calculates the sum of the numbers that make up the factorial
 */
class SumOfTheFactorial {
    private NumbersInput numbersInput;

    public SumOfTheFactorial(NumbersInput numbersInput) {
         this.numbersInput = numbersInput;
    }

    /**
     * This method calculates the sum of the numbers of the factorial
     *
     * @ factorialNumber - factorial of a number that the user enters.
     * @ arrayFactorial - array that records the number of factorial.
     * @ sumOfTheNumbersFact - variable that records the sum of the numbers of the factorial
     * */
    void findingTheSumOfTheFactorial() {
       BigInteger factorialNumber = factorialCalculation(numbersInput.getNumberInput());
       int  lengthArrayFactorial = factorialNumber.toString().length();
       int[] arrayFactorial = new int[lengthArrayFactorial];
       int sumOfTheNumbersFact = 0;
       int NUMBER_ZERO = 0;

        for (int i = NUMBER_ZERO; i < lengthArrayFactorial; i++) {
            arrayFactorial[i] = parseInt(factorialNumber.toString().charAt(i) + "");
        }

        for (int a = NUMBER_ZERO; a < arrayFactorial.length; a++) {
            sumOfTheNumbersFact += arrayFactorial[a];
        }
        System.out.println("Amount equal to the factorial of numbers --> " + sumOfTheNumbersFact);
    }
}