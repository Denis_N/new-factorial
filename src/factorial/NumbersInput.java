package factorial;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * executes the output of characters on the console
 */

class NumbersInput {
    private Validator validator ;
    private int numberInput;
    private String numberInputString;
    private BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    private String ERROR_MESSAGE = "You're mistaken! The number must contain only digits!";

       NumbersInput(Validator validator){
            this.validator = validator;
        }

       void enteringStringNumber() throws IOException {
           System.out.println("Enter the number, which is necessary to calculate the factorial of you!");
           while (true) {
              numberInputString = bufferedReader.readLine();
              if (validator.checkingInputCharacters(numberInputString)) {
                  numberInput = Integer.parseInt(numberInputString);
                  bufferedReader.close();
                  break;
              } else {
                 System.out.println(ERROR_MESSAGE);
              }
          }
       }

       public int getNumberInput() {
            return numberInput;

       }
}
